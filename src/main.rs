use poise::serenity_prelude as serenity;

type Error = Box<dyn std::error::Error + Send + Sync>;
type Context<'a> = poise::Context<'a, Data, Error>;
// User data, which is stored and accessible in all command invocations
struct Data {}

// Stuff for calendars
use almanac::*;
use std::fs::File;
use std::io::copy;
use std::io::BufReader;

// Write info about users in JSON file
use serde_json::json;
use std::collections::HashMap;
use std::io::prelude::*;

fn get_users() -> HashMap<String, String> {
    // Get the file
    let mut file = File::open("users.json").expect("Couldn't read file");
    let mut content = String::new();
    file.read_to_string(&mut content).unwrap();

    // Parse json
    println!("{}", "Read users.json and parse it");
    serde_json::from_str(&content).expect("Couldn't parse json")
}

async fn ics_calendar(user_id: String) -> Result<Calendar, String> {
    // Get list of users
    let users: HashMap<String, String> = get_users();

    if users.contains_key(&user_id) {
        // Get url
        let url = users.get(&user_id).unwrap();

        // Download the ICS file
        let response = reqwest::get(url).await.expect("File not found");
        let mut dest = File::create("/tmp/test.ics").expect("Couldn't create file");
        let content = response.text().await.unwrap();
        copy(&mut content.as_bytes(), &mut dest).expect("Couldn't write content");

        // Read file
        let buf = BufReader::new(File::open("/tmp/test.ics").expect("COuldn't read file"));
        println!("Download ICS and parse the calendar for {}", user_id);
        Ok(Calendar::parse(buf).unwrap())
    } else {
        Err("No link was found, please `/set` it before running this command".to_string())
    }
}

fn show_events(calendar: Result<Calendar, String>, days: i64, max_events: usize) -> String {
    match calendar {
        Err(m) => m,
        Ok(calendar) => {
            let now = Date::now();
            let last_time = now + Duration::days(days);
            let events: Vec<Event> = calendar
                .iter()
                .skip_while(|e| e.end_date() < now)
                .take_while(|e| e.start.same_day(&last_time) || e.start < last_time)
                .take(max_events)
                .collect();

            let day = Date::new();
            let mut message = String::from("```\n");
            for event in events {
                if !day.same_day(&event.start) {
                    message += &event.start.format("\n%a %b %e %Y\n");
                }
                message += &format!(
                    "{}-{} {}\n",
                    event.start.format("%R"),
                    event.end_date().format("%R"),
                    event.summary
                );
            }
            if message == "```\n" {
                message += "Aucun cours n'a été trouvé pour cette période.\n";
            }
            message += "```";
            message
        }
    }
}

#[poise::command(slash_command, prefix_command)]
async fn set(ctx: Context<'_>, #[description = "Url horarix"] url: String) -> Result<(), Error> {
    let mut users: HashMap<String, String> = get_users();

    // Add new element
    let id = ctx.author().id;
    users.insert(id.to_string(), url);

    // Serialize json
    let new_json = json!(users);
    let mut dest = File::create("users.json")?;
    dest.write_all(new_json.to_string().as_bytes())?;

    // Respond to user
    ctx.send(|c| c.content("URL added successfully").ephemeral(true))
        .await?;
    Ok(())
}

/// Afficher l'horaire de la semmaine
#[poise::command(slash_command, prefix_command)]
async fn week(ctx: Context<'_>) -> Result<(), Error> {
    let calendar = ics_calendar(ctx.author().id.to_string()).await;
    ctx.send(|c| c.content(show_events(calendar, 7, 100)).ephemeral(true))
        .await?;
    Ok(())
}

/// Afficher l'horaire pour le mois
#[poise::command(slash_command, prefix_command)]
async fn month(ctx: Context<'_>) -> Result<(), Error> {
    let calendar = ics_calendar(ctx.author().id.to_string()).await;
    ctx.send(|c| c.content(show_events(calendar, 31, 100)).ephemeral(true))
        .await?;
    Ok(())
}

/// Afficher l'horaire des prochaines 24h
#[poise::command(slash_command, prefix_command)]
async fn today(ctx: Context<'_>) -> Result<(), Error> {
    let calendar = ics_calendar(ctx.author().id.to_string()).await;
    ctx.send(|c| c.content(show_events(calendar, 0, 100)).ephemeral(true))
        .await?;
    Ok(())
}

/// Afficher le prochain cours
#[poise::command(slash_command, prefix_command)]
async fn next(ctx: Context<'_>) -> Result<(), Error> {
    let calendar = ics_calendar(ctx.author().id.to_string()).await;
    ctx.send(|c| c.content(show_events(calendar, 7, 2)).ephemeral(true))
        .await?;
    Ok(())
}

/// Savoir quand sont les vacances
#[poise::command(slash_command, prefix_command)]
async fn when(ctx: Context<'_>) -> Result<(), Error> {
    let calendar = ics_calendar(ctx.author().id.to_string()).await?;

    // Prends tous les cours qui sont à venir
    let events: Vec<Event> = calendar
        .iter()
        .skip_while(|e| e.start < Date::now())
        .collect();

    // Défini comme durée minimale de vacanaces 5 jours
    let minimum_vacances = Duration::days(5);

    // Commence par définir le debut des vacances à maintenant
    let mut debut_vacances = Date::now();
    for event in events {
        // Vérifie la différence entre la fin du cours et la var debut_vacances
        if event.end_date() - debut_vacances >= minimum_vacances {
            break;
        } else {
            debut_vacances = event.end_date();
        }
    }

    // Défini la différence de temps entre le debut des vacances et maintenant et le décompose en secondes
    let dans = debut_vacances - Date::now();
    let mut secondes = dans.num_seconds();

    // Décompose les secondes en jours, heures, minutes, secondes
    let jours: i64 = secondes / (60 * 60 * 24);
    secondes -= jours * 60 * 60 * 24;
    let heures: i64 = secondes / (60 * 60);
    secondes -= heures * 60 * 60;
    let minutes: i64 = secondes / 60;
    secondes -= minutes * 60;

    // Affiche le message "les vacances sont dans..." ou "les vacances sont maintenant..."
    if secondes != 0 {
        ctx.send(|c| {
            c.content(format!(
                "Les vacances sont dans {} jours, {} heures, {} minutes et {} secondes.",
                jours, heures, minutes, secondes
            ))
            .ephemeral(true)
        })
        .await?;
    } else {
        ctx.send(|c| {
            c.content("Les vacances sont MAINTENANT :tada:\nhttps://tenor.com/view/its-time-to-stop-stop-clock-time-gif-5001372")
                .ephemeral(true)
        })
        .await?;
    }

    Ok(())
}

#[poise::command(prefix_command)]
async fn register(ctx: Context<'_>) -> Result<(), Error> {
    poise::builtins::register_application_commands_buttons(ctx).await?;
    Ok(())
}

#[tokio::main]
async fn main() {
    match File::open("users.json") {
        Ok(_file) => println!("File exists, bot starting..."),
        Err(_e) => {
            let mut file = File::create("users.json").expect("Couldn't write to file");
            file.write_all(b"{}").unwrap();
            println!("File created, bot starting...");
        }
    }

    let framework = poise::Framework::builder()
        .options(poise::FrameworkOptions {
            commands: vec![set(), week(), today(), next(), register(), month(), when()],
            ..Default::default()
        })
        .token(std::env::var("DISCORD_TOKEN").expect("missing DISCORD_TOKEN"))
        .intents(serenity::GatewayIntents::non_privileged())
        .user_data_setup(move |_ctx, _ready, _framework| Box::pin(async move { Ok(Data {}) }));

    framework.run().await.unwrap();
}
